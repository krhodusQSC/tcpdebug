--[[
TCP Debug
Created by K. Rhodus - QSC
kevin.rhodus@qsc.com

Version History:
1.0 - Initial Build

]]--

-----------------------------------------------------------------------------------------------------------------------------------------
--      Socket Setup      --
----------------------------------------------------------------------------------------------------------------------------------------
sock = TcpSocket.New()
sock.ReadTimeout = 0
sock.WriteTimeout = 0
sock.ReconnectTimeout = 5

Controls.Status.Value = 5

-----------------------------------------------------------------------------------------------------------------------------------------
--      Socket EventHandler     --
-----------------------------------------------------------------------------------------------------------------------------------------

sock.EventHandler = function(sock, evt, err)
  if evt == TcpSocket.Events.Connected then
    print( "socket connected" )
    Controls.Status.Value = 0
    Controls.Status.String = "Socket Connected"
  elseif evt == TcpSocket.Events.Reconnect then
    print( "socket reconnecting..." )
    Controls.Status.Value = 3
    Controls.Status.String = "Socket Reconnecting"
  elseif evt == TcpSocket.Events.Data then
    print( "socket has data" )
    message = sock:Read(sock.BufferLength)
    while (message ~= nil) do
      print(message)
      Controls.IncomingData.String = Controls.IncomingData.String .."\n"..message
      print( "reading until CrLf got "..message )
      message = sock:Read(sock.BufferLength)
    end
  elseif evt == TcpSocket.Events.Closed then
    print( "socket closed by remote" )
    Controls.Status.Value = 3
    Controls.Status.String = "Socket Closed by Remote"
  elseif evt == TcpSocket.Events.Error then
    print( "socket closed due to error", err )
    Controls.Status.Value = 2
    Controls.Status.String = "Socket Closed Due to Error"
  elseif evt == TcpSocket.Events.Timeout then
    print( "socket closed due to timeout" )
    Controls.Status.Value = 3
    Controls.Status.String = "Socket Closed Due to Timeout"
  else
    print( "unknown socket event", evt ) --should never happen
    Controls.Status.Value = 2
    Controls.Status.String= "Unknown Socket Event"
  end
end



-----------------------------------------------------------------------------------------------------------------------------------------
--      Script Functions     --
-----------------------------------------------------------------------------------------------------------------------------------------
--Socket Connect

Controls.Connect.EventHandler = function(ctl)
  if ctl.Boolean then
    ctl.Legend = "Disconnect"
    sock:Connect(Controls.IPAddress.String, tonumber(Controls.Port.String))
  else
    sock:Disconnect()
    ctl.Legend = "Connect"
    Controls.Status.Value = 3
    Controls.Status.String = "Disconnected"
  end
end

--Send Data
Controls.Send.EventHandler = function()

  sock:Write(Controls.OutgoingData.String..SelectedEOL)
end


--Check on Startup
  if Controls.Connect.Boolean then
    sock:Connect(Controls.IPAddress.String, tonumber(Controls.Port.String))
  else
    sock:Disconnect()
  end


-----------------------------------------------------------------------------------------------------------------------------------------
--      EOL Characters     --
-----------------------------------------------------------------------------------------------------------------------------------------

--Create Lists

EOLList = {"Cr", "Lf", "CrLf", "None"}
EOLCharacters = {"\x0D", "\x0A", "\x0D\x0A", ""}
Controls.EOL.Choices = EOLList

--Selected EOL

CheckEOL = function()
  for idx,ctl in ipairs(EOLList) do
    if ctl == Controls.EOL.String then
      SelectedEOL = EOLCharacters[idx]
    end
  end
  print(SelectedEOL)
end

Controls.EOL.EventHandler = CheckEOL
CheckEOL()


-----------------------------------------------------------------------------------------------------------------------------------------
--      Clear Button     --
-----------------------------------------------------------------------------------------------------------------------------------------


Controls.Clear.EventHandler = function()
  Controls.IncomingData.String = ""
end







-------------------
--[[

Copyright 2021 QSC, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this softwareand associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

]]--