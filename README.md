## TCP Debug

This user component is a simple way to debug TCP commands from within a Q-SYS Design. Simply add it to your design and you are now able to establish two way communication with a remote TCP device. 

Preview:  
![picture](Screenshot.PNG)  